# Assignment: Authentication Using Laravel

The task is work on Laravel authentication

Tutorial URL: https://laravel.com/docs/authentication

### Task

1. Create a Laravel application
2. Develop signup/registration form. Fields for registration are:
    1. Name
    2. Email (This will be unique)
    3. Password
3. On registration send an email to the registered user. This will be a welcome email for the user. 
    * Subject: Welcome {{Name of user}}
    * Body
        ```text
        Hello {{Name of user}}
  
        Welcome to the portal and thank you for registering with us.
  
        You can learn more about us at https://symbtechnologies.com/      
  
        Thanks and Regards,
        Team SYMB
        ```
    You can take help of this document to implement mailer: https://laravel.com/docs/mail
4. Now develop a login form with user to login into the application using
    * Email
    * Password
5. Not create a column in use table to get the phone number of the user. 
6. On successful login ask user to enter 10 digit phone number, and save it into the columen created at step 5
7. Edit the login functionality to login user into the application by either of the following method:
    * Email and password
    * 10 digit phone number and password
8. Integrate this package in laravel to implement email verification: https://github.com/jrean/laravel-user-verification. You can also use this URL for implementation: https://laravel.com/docs/verification




