# Laravel Assignments

_By SYMB Technolgoies_

1. [Laravel Authentication](https://gitlab.com/symb-assessment/laravel-assignments/blob/master/assignments/laravel_authentication.md)
2. [Export Using Laravel](https://gitlab.com/symb-assessment/laravel-assignments/blob/master/assignments/assignment_laravel_export.md)
3. [Roles and Permissions in Laravel](https://gitlab.com/symb-assessment/laravel-assignments/blob/master/assignments/roles_and_permissions.md)