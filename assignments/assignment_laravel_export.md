# Assignment: Exports Using Laravel

The task is to export a list in excel in Laravel

### Task

1. Create a Laravel application
2. Call a rest API GET:https://restcountries.eu/rest/v2/all to get the list of countries
3. List the countries as per this UI: https://symb-assessment.gitlab.io/laravel-assignments/uis/countries_export_assignment.html
4. Install [Laravel Excel](https://laravel-excel.com/) or any other package to export the countries list from laravel to an Excel file using the button 'Export' on the UI
