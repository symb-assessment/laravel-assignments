# Assignment: Authentication Using Laravel

The task is work on Laravel authentication

Tutorial URL: https://laravel.com/docs/authentication

### Task

1. Create a Laravel application
2. Develop signup/registration form. Fields for registration are:
    1. Name
    2. Email (This will be unique)
    3. Password
3. On registration send an email to the registered user. This will be a welcome email for the user. 
    * Subject: Welcome {{Name of user}}
    * Body
        ```text
        Hello {{Name of user}}
  
        Welcome to the portal and thank you for registering with us.
  
        You can learn more about us at https://symbtechnologies.com/      
  
        Thanks and Regards,
        Team SYMB
        ```
    You can take help of this document to implement mailer: https://laravel.com/docs/mail
4. Now develop a login form with user to login into the application using
    * Email
    * Password
5. Install one of the below listed package to implement roles and permission management of users:
    * Entrust: https://github.com/Zizaco/entrust
    * Spatie: https://github.com/spatie/laravel-permission
6. Now write a seeder (https://laravel.com/docs/seeding) to create following roles in the application:
    * Administrator
    * Manager
    * User
7. Now create following users in the application using seeder:
    * Administrator user:
        * Name: Administrator
        * Email: admin@example.com
        * Password: admin123
        * Role: Administrator
    * Manager user:
        * Name: Sales Manager
        * Email: manager@example.com
        * Password: manager123
        * Role: Manager
    * User:
        * Name: John Deo
        * Email: user@example.com
        * Password: user123
        * Role: User
    * User:
        * Name: Thalaiva
        * Email: thalaiva@example.com
        * Password: thalaiva123
        * Role: Manager and Administrator
8. Create a URL: '/manage' which will be accessible to authenticated user having role **Administrator**
9. Create a URL: '/manage' which will be accessible to authenticated user having role **Manager**
10. Create a URL: '/home' which will be accessible to authenticated user having role any role
